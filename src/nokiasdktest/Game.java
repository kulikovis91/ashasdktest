/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nokiasdktest;

import java.util.Random;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.lcdui.game.TiledLayer;

/**
 *
 * @author Ivan
 */
class Game extends GameCanvas {
    private int touchX, touchY;
    private Vector units;
    private Random r;   
    private TiledLayer layer;
    private Graphics g;
    private Unit currUnit;    
    private Midlet m;
    private int displayWidth;
    private int displayHeight;
    public Unit bunker;
    public int kills = 0;  
    int seed = 3;
    int speed = -3;
    
    public Game(Midlet m) {
        super(true);        
        displayHeight = getHeight();
        displayWidth = getWidth();
        try {             
            this.m = m;
            r = new Random(65465465);
            units = new Vector(1000);            
            // layer = new TiledLayer(8, 10, Resources.grass, Resources.grass.getWidth(), Resources.grass.getHeight());
            layer = new TiledLayer(displayWidth / Resources.grass.getWidth() + 1, displayHeight / Resources.grass.getHeight() + 1, Resources.grass, Resources.grass.getWidth(), Resources.grass.getHeight());
            layer.fillCells(0, 0, displayWidth / Resources.grass.getWidth() + 1, displayHeight / Resources.grass.getHeight() + 1, 1);                            
            g = getGraphics();   
            bunker = new Unit(Resources.bunker_shooting, 96, 64);
            bunker.setPosition(displayWidth / 2 - 48, 8);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    protected void update() {               
        layer.paint(g);  
        bunker.paint(g);
        if (bunker.getFrame() != 0) {
            bunker.nextFrame();
        }
        
        g.drawString("Kills: " + String.valueOf(kills), 0, 0, g.TOP | g.LEFT);
        for (int i = 0; i < units.size(); i++) {
            currUnit = (Unit)units.elementAt(i);   
            if (!currUnit.dead && !currUnit.collidesWith(bunker, true))
                currUnit.move(0, speed);
            else if (currUnit.collidesWith(bunker, true) && bunker.health > 0) {
                bunker.health--;
            }
            
            currUnit.nextFrame();
            currUnit.paint(g);
                
            if (currUnit.getY() < -64) {
                units.removeElement(currUnit);
            }
            else if (currUnit.dead && currUnit.getFrame() == 6) {
                units.removeElement(currUnit);
            }

            bunker.displayHealth(g);
        }
        flushGraphics();
        
        if (bunker.health == 0) {
                m.spawn.cancel();
                m.run.cancel();
                m.gc.cancel();
                try {
                    g.drawImage(Image.createImage("/gameover.png"), 0, 0, g.TOP | g.LEFT);
                    g.setColor(0, 255, 0);
                    g.drawString("KILLS: " + String.valueOf(kills), 96, 160, g.TOP | g.LEFT);
                    flushGraphics();
                }
                catch (Exception e) { }
        }       
        
        if (kills != 0 && kills % 250 == 0) {
            speed -= 1;
            kills++;
        }            
        if (kills != 0 && kills % 1000 == 0) {
            seed += 1;
            kills++;
        }
    }       
    
    protected void pointerPressed(int x, int y){
        touchX = x;
        touchY = y;   
        if (bunker.health != 0) {
            for (int i = 0; i < units.size(); i++) {
                currUnit = (Unit)units.elementAt(i);
                if (currUnit.contains(x, y) && !currUnit.dead) {
                    currUnit.hit();
                    kills++;
                }
            }        
            bunker.setFrame(1);
        }
        else {
            m.notifyDestroyed();
        } 
    }
    

    protected void pointerDragged(int x, int y) {
        touchX = x;
        touchY = y;
    }
    
    protected void spawn()
    {
        for (int i = 0; i < seed; i++) {
            currUnit = new Unit(Resources.zerg_up, 64, 64);
            currUnit.setPosition(r.nextInt(displayWidth) - 32 , displayHeight);
            currUnit.setFrameSequence(new int[] {0,1,2,3,4,5,6,7,8,9});
            units.addElement(currUnit);    
        }            
    }
};
