/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nokiasdktest;

import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Display;
import javax.microedition.midlet.*;

/**
 * @author Ivan
 */
public class Midlet extends MIDlet {       
       
    Game g;
    Timer t;
    
    TimerTask run = new TimerTask() {
        public void run() {            
            g.update();
        }
    };
    TimerTask spawn = new TimerTask() {
        public void run() {
            g.spawn();
        }
    };
    
    TimerTask gc = new TimerTask() {

        public void run() {
            Runtime.getRuntime().gc();
        }
    };

    public void startApp() {
        Resources.init();
        g = new Game(this);        
        Display disp = Display.getDisplay(this);
        disp.setCurrent(g);                
        t = new Timer();
        t.scheduleAtFixedRate(run, 0, 60);   
        t.scheduleAtFixedRate(spawn, 0, 500);
        t.scheduleAtFixedRate(gc, 0, 10000); 
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    }
}
