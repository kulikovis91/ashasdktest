/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nokiasdktest;

import javax.microedition.lcdui.Image;

/**
 *
 * @author Ivan
 */
public class Resources {
    public static Image zerg_up;
    public static Image zerg_up_right;
    public static Image zerg_right;
    public static Image zerg_down_right;
    public static Image zerg_down;
    public static Image death;
    public static Image bunker;
    public static Image bunker_shooting;
    public static Image grass;
    
    public static void init() {
        try {
           zerg_up = Image.createImage("/zerg/zerg_up.png");
           zerg_up_right = Image.createImage("/zerg/zerg_up_right.png");
           zerg_right = Image.createImage("/zerg/zerg_right.png");
           zerg_down_right = Image.createImage("/zerg/zerg_down_right.png");
           zerg_down = Image.createImage("/zerg/zerg_down.png");
           death = Image.createImage("/zerg/death.png"); 
           bunker = Image.createImage("/bunker.png");
           bunker_shooting = Image.createImage("/bunker_shooting.png");
           grass =  Image.createImage("/grass.jpg");
        }
        catch (Exception e) { }        
    }     
}

