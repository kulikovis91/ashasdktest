/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nokiasdktest;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;

/**
 *
 * @author Ivan
 */
public class Unit extends Sprite {
    public boolean dead;
    public int health = 1000;
    
    public Unit(Image img, int w, int h) {
        super(img, w, h);
        dead = false;
    }        
    
    public void hit() {
        dead = true;
        setImage(Resources.death, 64, 64);
        setFrameSequence(new int[] {0,1,2,3,4,5,6});
    }
    
    public boolean contains(int _x, int _y) {        
        return ((_x > getX() && _x < getX() + 64) && (_y > getY() && _y < getY() + 64));
    }
    
    public void displayHealth (Graphics g) {
        int col = g.getColor();
        if (health > 750)
            g.setColor(0, 255, 0);
        else if (health > 250 && health < 750)
            g.setColor(255, 255, 0);
        else if (health < 250)
            g.setColor(255, 0, 0);
        g.fillRect(getX(), getY() - 5, health / 10, 5);
        g.setColor(col);
    } 
}
